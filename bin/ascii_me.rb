load 'lib/ascii_digits.rb'
load 'lib/read_file.rb'
load 'lib/convert.rb'
load 'lib/backwards.rb'
require 'json'

if ARGV[0] # File input
  file = ARGV[0]
else
  puts "Ints file please, (probably `ints.txt`)"
  file = gets.strip
end

read_file(file)
convert(@ints)
back_to_int(@asciis)
