def convert(ints)
  puts "Converting Ints to ASCIIS\n"
  output_file = "export/#{Time.now.to_i}"
  `touch #{output_file}`
  @asciis = []
  @faults = []

  ints.each do |i|

    if !/\A\d{1}\z/.match(i)
      puts "NOT GOOD"
      @faults << i
    else
      i = i.to_i
      ascii = @ascii_digits[i]
      puts ascii
      `echo '#{ascii}' >> #{output_file}`
      @asciis << ascii
    end

  end
  fault_count = @faults.size
  @results = {proper_count: ints.size -  fault_count, fault_count: fault_count}
  json = JSON.generate(@results)
  puts "json:\n#{json}"
end
