# ASCII Me

load text file with ints to be output as their ASCII equivalent

## Getting Started

Run with the bundled ints file
```
ruby bin/ascii_me.rb ints.txt
```
